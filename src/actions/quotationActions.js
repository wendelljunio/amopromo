import { SET_DESTINATION, SET_COVERAGE_BEGIN, SET_COVERAGE_END, SET_QUOTATION_GENERATED, SET_PRODUCT_MATCH } from './actionType';

export const setDestination = payload => {
	return { 
		type: SET_DESTINATION,
		payload
	}
}

export const setCoverageBegin = payload => {
	return {
		type: SET_COVERAGE_BEGIN,
		payload
	}
}

export const setCoverageEnd = payload => {
	return {
		type: SET_COVERAGE_END,
		payload
	}
}

export const setQuotationGenerated = payload => {
	return {
		type: SET_QUOTATION_GENERATED,
		payload
	}
}

export const setProductMatch = payload => {
	return {
		type: SET_PRODUCT_MATCH,
		payload
	}
}
