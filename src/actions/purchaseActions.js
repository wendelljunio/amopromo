import { SET_EXTERNAL_ID, SET_PLAN_ID, SET_COVERAGE_BEGIN, SET_COVERAGE_END, SET_DESTINATION_ID, SET_CONTACT, SET_ADDRESS, SET_PLAN_ID_SELECTED, SET_INSUREDS } from '../actions/actionType';

export const setPlanIdSelected = payload => {
  return {
    type: SET_PLAN_ID_SELECTED,
    payload
  }
}

export const setExternalId = payload => {
  return {
    type: SET_EXTERNAL_ID,
    payload
  }
}

export const setPlanId = payload => {
  return {
    type: SET_PLAN_ID,
    payload
  }
}

export const setCoverageBegin = payload => {
  return {
    type: SET_COVERAGE_BEGIN,
    payload
  }
}

export const setCoverageEnd = payload => {
  return {
    type: SET_COVERAGE_END,
    payload
  }
}

export const setDestinationId = payload => {
  return {
    type: SET_DESTINATION_ID,
    payload
  }
}

export const setContact = payload => {
  return {
    type: SET_CONTACT,
    payload
  }
}

export const setAddress = payload => {
  return {
    type: SET_ADDRESS,
    payload
  }
}

export const setInsureds = payload => {
  return {
    type: SET_INSUREDS,
    payload
  }
}