import {  SET_PRODUCTS, SET_DESTINATIONS } from './actionType';

export const setProducts = payload => {
  return {
      type: SET_PRODUCTS,
      payload
  }
}

export const setDestinations = payload => {
  return {
      type: SET_DESTINATIONS,
      payload
  }
}