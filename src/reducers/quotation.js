import { SET_DESTINATION, SET_COVERAGE_BEGIN, SET_COVERAGE_END, SET_QUOTATION_GENERATED, SET_PRODUCT_MATCH } from '../actions/actionType';

const initialState = {
	quotationState: {
		destination: '',
		coverageBegin: '',
		coverageEnd: ''
	},
	quotationGenerated: [],
	productMatch: []
};

export default (state = initialState, action) => {
	switch (action.type) {
		case SET_DESTINATION:
			return { ...state, quotationState: { ...state.quotationState, destination: action.payload }};
		case SET_COVERAGE_BEGIN:
			return { ...state, quotationState: { ...state.quotationState, coverageBegin: action.payload }};
		case SET_COVERAGE_END: 
			return { ...state, quotationState: { ...state.quotationState, coverageEnd: action.payload }};
		case SET_QUOTATION_GENERATED:
			return { ...state, quotationGenerated: action.payload };
		case SET_PRODUCT_MATCH:
			return { ...state, productMatch: action.payload };
		default:
			return state;
	}
}
