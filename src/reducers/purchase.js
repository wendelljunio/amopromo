import { SET_EXTERNAL_ID, SET_PLAN_ID, SET_COVERAGE_BEGIN, SET_COVERAGE_END, SET_DESTINATION_ID, SET_CONTACT, SET_ADDRESS, SET_PLAN_ID_SELECTED, SET_INSUREDS } from '../actions/actionType';

const initialState = {
  data: {
    external_id: 123,
    plan_id: 0,
    coverageBegin: '',
    coverageEnd: '',
    destinationId: 0,
    contact: {
      name: '',
      email: '',
      phone: ''
    },
    address: {
      address: '',
      cep: '',
      city: '',
      state: ''
    },
    insureds: []
  },
  insured: {
    external_id: 0,
    first_name: '',
    last_name: '',
    date_of_birth: '',
    cpf: ''
  },
  planIdSelected: 0
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_PLAN_ID_SELECTED: 
      return { ...state, planIdSelected: action.payload };
    case SET_EXTERNAL_ID:
      return {...state, data: { ...state.data, external_id: action.payload }};
    case SET_PLAN_ID:
      return {...state, data: { ...state.data, plan_id: action.payload }};
    case SET_COVERAGE_BEGIN:
      return {...state, data: { ...state.data, coverageBegin: action.payload }};
    case SET_COVERAGE_END:
      return {...state, data: { ...state.data, coverageEnd: action.payload }};
    case SET_DESTINATION_ID:
      return {...state, data: { ...state.data, destinationId: action.payload }};
    case SET_CONTACT:
      return { ...state, data: { ...state.data, contact: action.payload }};
    case SET_ADDRESS:
      return { ...state, data: { ...state.data, address: action.payload }};
    case SET_INSUREDS: {
      state.data.insureds.push(action.payload)
      return { ...state, insured: 
        { ...state.insured, first_name: action.payload.first_name, last_name: action.payload.last_name,
          date_of_birth: action.payload.date_of_birth, cpf: action.payload.cpf 
        }
      }
    }
    default:
      return state;
  }
}
