import { combineReducers } from 'redux';
import quotation from './quotation';
import base from './base';
import purchase from './purchase';

export default combineReducers({
    quotation,
    base,
    purchase
});