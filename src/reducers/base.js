import { SET_PRODUCTS, SET_DESTINATIONS } from '../actions/actionType';

const initialState = {
    destinations: [],
    products: []
}

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_PRODUCTS:
            return { ...state, products: action.payload };
        case SET_DESTINATIONS:
            return { ...state, destinations: action.payload };
        default:
            return state;
    }
}