import React, { Component } from 'react';
import { client } from '../../client/client';
import { connect } from 'react-redux';
import { Formik, Field, Form, ErrorMessage, getIn } from 'formik';
import * as Yup from 'yup';
import * as purchaseActions from '../../actions/purchaseActions';
import Card from '../../components/Card';
import FormGroup from '../../components/Form/FormGroup';
import Row from '../../components/Layout/Row';
import Col from '../../components/Layout/Col';

class Purchase extends Component {
  constructor(props) {
    super(props);
    this.state = {
      infoPurchase: null
    }
    this.handlePurchase = this.handlePurchase.bind(this);
  }

  componentDidMount(){
    if(!this.props.planIdSelected || !this.props.coverageBegin || !this.props.coverageEnd || !this.props.destination ){
      this.props.history.push('/cotacao')
    } else {
      this.props.setExternalId(987)
      this.props.setPlanId(this.props.planIdSelected)
      this.props.setCoverageBegin(this.props.coverageBegin)
      this.props.setCoverageEnd(this.props.coverageEnd)
      this.props.setDestinationId(this.props.destination)
    }
  }

  handlePurchase() {
    client.post('purchase', this.props.dataPurchase)
    .then(response => {
      this.setState({infoPurchase: response.data})
      console.log(response.data)
    })
    .catch(e => {
      this.setState({infoPurchase: e.response.data})
      console.log(e)
    })
  }

  render(){
    return (
    <Formik
     initialValues={{
        contact: {
          name: '',
          email: '',
          phone: ''
        },
        address: {
          address: '',
          cep: '',
          city: '',
          state: ''
        },
        insured: {
          firtsName: '',
          lastName: '',
          dateOfBirth: '',
          cpf: ''
        }
      }}
      enableReinitialize
      validationSchema={Yup.object().shape({
        contact: Yup.object().shape({
          name: Yup.string()
            .required('O campo nome é obrigatorio!'),
          phone: Yup.string()
            .min(10, 'Insira um numero valido com DDD!')
            .max(11, 'Insira um numero valido com DDD!')
            .required('O campo telefone é obrigatorio!'),
          email: Yup.string()
            .email('Email invalido !')
            .required('Email é obrigatorio!'),
        }),
        address: Yup.object().shape({
          address: Yup.string()
            .required('O endereço é obrigatorio!'),
          cep: Yup.string()
            .length(8, 'Insira um cep valido!')
            .required('O campo cep é obrigatorio!'),
          city: Yup.string()
            .required('O campo cidade é obrigatorio!'),
          state: Yup.string()
            .required('O campo estado é obrigatorio!'),
        }),
        insured: Yup.object().shape({
          firtsName: Yup.string()
            .required('O campo nome é obrigatorio'),
          lastName: Yup.string()
            .required('O campo sobrenome é obrigatorio'),
          dateOfBirth: Yup.date()
            .max(new Date(), 'A data de nascimento não pode ser maior do que a data atual!')
            .required('A data de embarque é obrigatoria!'),
          cpf: Yup.string()
            .length(11, 'Insira um CPF valido!')
            .required('O campo CPF é obrigaorio')
        })
      })}
      onSubmit={fields => {
        this.props.setContact(fields.contact)
        this.props.setAddress(fields.address)
        this.props.setInsureds({
          'external_id': this.props.external_id,
          'firts_name': fields.insured.firtsName,
          'last_name': fields.insured.lastName,
          'date_of_birth': fields.insured.dateOfBirth,
          'cpf': fields.insured.cpf
        })
          this.handlePurchase()
      }}
      render={({errors, touched }) => (
        <div>
          <h1 className="text-white">Comprar seguro</h1>
          <Card>
            <Form>
              <h3>Contato</h3>
              <Row>
                <Col sm="12" md="12" lg="12" xl="12">
                  <FormGroup>
                    <label htmlFor="name">Nome</label>
                    <Field name="contact.name" id="contact.name" type="text" className={'form-control' + (getIn(errors, 'contact.name') && getIn(touched, 'contact.name') ? ' is-invalid' : '')} />
                    <ErrorMessage name="contact.name" component="div" className="invalid-feedback" />
                  </FormGroup>
                </Col>
                <Col sm="12" md="8" lg="6" xl="6">
                  <FormGroup>
                    <label htmlFor="email">Email</label>
                    <Field name="contact.email" id="email" type="text" className={'form-control' + (getIn(errors, 'contact.email') && getIn(touched, 'contact.email') ? ' is-invalid' : '')} />
                    <ErrorMessage name="contact.email" component="div" className="invalid-feedback" />
                  </FormGroup>
                </Col>
                <Col sm="12" md="4" lg="6" xl="6">
                  <FormGroup>
                    <label htmlFor="phone">Telefone</label>
                    <Field name="contact.phone" id="phone" type="text" className={'form-control' + (getIn(errors, 'contact.phone') && getIn(touched, 'contact.phone') ? ' is-invalid' : '')} />
                    <ErrorMessage name="contact.phone" component="div" className="invalid-feedback" />
                  </FormGroup>
                </Col>
              </Row>
              <hr />
              <h3>Endereço</h3>
              <Row>
                <Col sm="12" md="12" lg="12" xl="12">
                  <FormGroup>
                    <label htmlFor="address">Endereço</label>
                    <Field name="address.address" id="address" type="text" className={'form-control' + (getIn(errors, 'address.address') && getIn(touched, 'address.address') ? ' is-invalid' : '')} />
                    <ErrorMessage name="address.address" component="div" className="invalid-feedback" />
                  </FormGroup>
                </Col>
                <Col sm="12" md="4" lg="2" xl="2">
                  <FormGroup>
                    <label htmlFor="address.cep">CEP</label>
                    <Field name="address.cep" id="cep" type="text" className={'form-control' + (getIn(errors, 'address.cep') && getIn(touched, 'address.cep') ? ' is-invalid' : '')} />
                    <ErrorMessage name="address.cep" component="div" className="invalid-feedback" />
                  </FormGroup>
                </Col>
                <Col sm="12" md="4" lg="5" xl="5">
                  <FormGroup>
                    <label htmlFor="city">Cidade</label>
                    <Field name="address.city" id="city" type="text" className={'form-control' + (getIn(errors, 'address.city') && getIn(touched, 'address.city') ? ' is-invalid' : '')} />
                    <ErrorMessage name="address.city" component="div" className="invalid-feedback" />
                  </FormGroup>
                </Col>
                <Col sm="12" md="4" lg="5" xl="5">
                  <FormGroup>
                    <label htmlFor="state">Estado</label>
                    <Field name="address.state" id="state" type="text" className={'form-control' + (getIn(errors, 'address.state') && getIn(touched, 'address.state') ? ' is-invalid' : '')} />
                    <ErrorMessage name="address.state" component="div" className="invalid-feedback" />
                  </FormGroup>
                </Col>
              </Row>
              <hr />
              <h3>Segurados</h3>
              <Row>
                <Col sm="12" md="2" lg="2" xl="2">
                  <FormGroup>
                    <label htmlFor="firtsName">Nome</label>
                    <Field name="insured.firtsName" id="firtsName" type="text" className={'form-control' + (getIn(errors, 'insured.firtsName') && getIn(touched, 'insured.firtsName') ? ' is-invalid' : '')} />
                    <ErrorMessage name="insured.firtsName" component="div" className="invalid-feedback" />
                  </FormGroup>
                </Col>
                <Col sm="12" md="5" lg="5" xl="5">
                  <FormGroup>
                    <label htmlFor="lastName">Sobrenome</label>
                    <Field name="insured.lastName" id="lastName" type="text" className={'form-control' + (getIn(errors, 'insured.lastName') && getIn(touched, 'insured.lastName') ? ' is-invalid' : '')} />
                    <ErrorMessage name="insured.lastName" component="div" className="invalid-feedback" />
                  </FormGroup>
                </Col>
                <Col sm="12" md="3" lg="3" xl="3">
                  <FormGroup>
                    <label htmlFor="dateOfBirth">Data de Nascimento</label>
                    <Field name="insured.dateOfBirth" id="dateOfBirth" type="date" className={'form-control' + (getIn(errors, 'insured.dateOfBirth') && getIn(touched, 'insured.dateOfBirth') ? ' is-invalid' : '')} />
                    <ErrorMessage name="insured.dateOfBirth" component="div" className="invalid-feedback" />
                  </FormGroup>
                </Col>
                <Col sm="12" md="2" lg="2" xl="2">
                  <FormGroup>
                    <label htmlFor="cpf">CPF</label>
                    <Field name="insured.cpf" id="cpf" type="text" className={'form-control' + (getIn(errors, 'insured.cpf') && getIn(touched, 'insured.cpf') ? ' is-invalid' : '')} />
                    <ErrorMessage name="insured.cpf" component="div" className="invalid-feedback" />
                  </FormGroup>
                </Col>
              </Row>
              <button type="submit" className="btn btn-outline-info btn-block" >Comprar seguro</button>
            </Form>
          </Card>
          <br />
          { this.state.infoPurchase ?
          <Card>
            <p>{this.state.infoPurchase}</p>
          </Card> : ''
          }
        </div>
      )}
    />
    )
  }
}

const mapStateToProps = store => ({
  dataPurchase: store.purchase.data,
  insured: store.purchase.insured,
  external_id: store.purchase.data.external_id,
  planIdSelected: store.purchase.planIdSelected,
  coverageBegin: store.quotation.quotationState.coverageBegin,
  coverageEnd: store.quotation.quotationState.coverageEnd,
  destination: store.quotation.quotationState.destination,
});

const mapDispatchToProps = dispatch => ({
  setExternalId: (value) => dispatch(purchaseActions.setExternalId(value)),
  setPlanId: (value) => dispatch(purchaseActions.setPlanId(value)),
  setCoverageBegin: (value) => dispatch(purchaseActions.setCoverageBegin(value)),
  setCoverageEnd: (value) => dispatch(purchaseActions.setCoverageEnd(value)),
  setDestinationId: (value) => dispatch(purchaseActions.setDestinationId(value)),
  setContact: (value) => dispatch(purchaseActions.setContact(value)),
  setAddress: (value) => dispatch(purchaseActions.setAddress(value)),
  setInsureds: (value) => dispatch(purchaseActions.setInsureds(value))
});

export default connect(mapStateToProps, mapDispatchToProps) (Purchase);