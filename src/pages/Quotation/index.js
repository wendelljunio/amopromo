import  React, { Component } from 'react';
import { connect } from 'react-redux';
import { client } from '../../client/client';
import * as quotationActions from '../../actions/quotationActions'
import * as baseActions from '../../actions/baseActions';
import { Formik, Field, Form, ErrorMessage, getIn } from 'formik';
import * as Yup from 'yup';
import Card from '../../components/Card/index';
import FormGroup from '../../components/Form/FormGroup';
import Row from '../../components/Layout/Row';
import Col from '../../components/Layout/Col';
import TableQuotation from './TableQuotation';

class Quotation extends Component {
  constructor(props) {
    super(props);
    this.handleQuotation = this.handleQuotation.bind(this);
  }
  
  componentDidMount() {
    this.props.setQuotationGenerated([])
    this.props.setDestination('')
    this.props.setCoverageBegin('')
    this.props.setCoverageEnd('')
    client.get('base/destinations')
    .then(response => {
      this.props.setDestinations(response.data)
    })
    .catch(e => {
      console.log(e)
    })
  }

  handleQuotation(){
    this.props.setQuotationGenerated([]);
    client.post('quotation', {
      coverage_begin: this.props.coverageBegin,
      coverage_end: this.props.coverageEnd,
      destination: parseInt(this.props.destination),
    }).then(response => {
      if(response.data){
        this.props.setQuotationGenerated(response.data)
      }
    })
    .catch(e => {
      console.log(e)
    })
  }

  render() {
    return (
      <Formik
        initialValues={this.props.quotationState}
        enableReinitialize
        validationSchema={Yup.object().shape({
          destination: Yup.string()
            .required('O destino é obrigatorio'),
          coverageBegin: Yup.date()
            .min(new Date(), 'A data de embarque não pode ser menor ou igual a data atual')
            .required('A data de embarque é obrigatoria!'),
          coverageEnd: Yup.date()
            .min(Yup.ref('coverageBegin'), 'A data de desembarque não pode ser menor que a de embarque')
            .required('A data de desembarque é obrigatoria!')
          })
        }
        onSubmit={fields => {
            this.props.setDestination(fields.destination)
            this.props.setCoverageBegin(fields.coverageBegin)
            this.props.setCoverageEnd(fields.coverageEnd)
            this.handleQuotation()
        }}
        render={({errors, status, touched }) => (
          <div>
            <h1 className="text-white">Faça uma cotação </h1>
            <Card sizeCard="w-50">
              <Form>
                <Row>
                  <Col sm="12" md="12" lg="12" xl="12">
                    <FormGroup>
                      <Field name="destination" component="select" className={'form-control' + (getIn(errors, 'destination') && getIn(touched, 'destination') ? ' is-invalid' : '')} >
                        <option value="">Selecione um destino</option>
                        {this.props.destinations.map(destination => <option key={destination.id} value={destination.id}>{destination.name}</option>)}
                      </Field>
                    </FormGroup>
                  </Col>
                  <Col sm="12" md="6" lg="6" xl="6">
                    <FormGroup>
                      <label htmlFor="begin" >Data embarque</label>
                      <Field name="coverageBegin" id="begin" type="date" className={'form-control' + (getIn(errors, 'coverageBegin') && getIn(touched, 'coverageBegin') ? ' is-invalid' : '')} />
                      <ErrorMessage name="coverageBegin" component="div" className="invalid-feedback" />
                    </FormGroup>
                  </Col>
                  <Col sm="12" md="6" lg="6" xl="6">
                    <FormGroup>
                      <label htmlFor="end">Data desembarque</label>
                      <Field name="coverageEnd" id="end" type="date" className={'form-control' + (getIn(errors, 'coverageEnd') && getIn(touched, 'coverageEnd') ? ' is-invalid' : '')} />
                      <ErrorMessage name="coverageEnd" component="div" className="invalid-feedback" />
                    </FormGroup>
                  </Col>
                </Row>
                <button type="submit" className="btn btn-outline-info btn-block">Cotar</button>
              </Form>
            </Card>
            <br />
            { this.props.quotationGenerated.length > 0 ? <TableQuotation /> : ''}
          </div>
        )}
      />
    );
  }
}

const mapStateToProps = store => ({
  quotationState: store.quotation.quotationState,
  destination: store.quotation.quotationState.destination,
  coverageBegin: store.quotation.quotationState.coverageBegin,
  coverageEnd: store.quotation.quotationState.coverageEnd,
  quotationGenerated: store.quotation.quotationGenerated,
  destinations: store.base.destinations
});

const mapDispatchToProps = dispatch => ({
  setDestination: (value) => dispatch(quotationActions.setDestination(value)),
  setCoverageBegin: (value) => dispatch(quotationActions.setCoverageBegin(value)),
  setCoverageEnd: (value) => dispatch(quotationActions.setCoverageEnd(value)),
  setQuotationGenerated: (value) => dispatch(quotationActions.setQuotationGenerated(value)),
  setDestinations: (value) => dispatch(baseActions.setDestinations(value))
});

export default connect(mapStateToProps, mapDispatchToProps) (Quotation);