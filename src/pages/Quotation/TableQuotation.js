import  React, { Component } from 'react';
import { connect } from 'react-redux';
import { client } from '../../client/client';
import { withRouter } from 'react-router-dom'
import * as quotationActions from '../../actions/quotationActions'
import * as baseActions from '../../actions/baseActions';
import * as purchaseActions from '../../actions/purchaseActions';
import Table from '../../components/Table';
import Card from '../../components/Card/index';

class TableQuotation extends Component {
  constructor(props) {
    super(props);
    this.getProductsAPI = this.getProductsAPI.bind(this);
    this.productQuotationMatch = this.productQuotationMatch.bind(this);
    this.handlePlan = this.handlePlan.bind(this);
  }

  handlePlan(planId){
    this.props.setPlanIdSelected(planId)
    this.props.history.push('/compra')
  }

  async componentDidMount() {
    await this.props.setProductMatch([])
    await this.getProductsAPI();
    await this.productQuotationMatch();
  }

  async getProductsAPI(){
    await client.get('base/products')
    .then(response => {
      this.props.setProducts(response.data)
    })
    .catch(e => {
      console.log(e)
    })
  }

  async productQuotationMatch() {
    await this.props.setProductMatch(this.props.products.filter(
      product => product.id === this.props.quotationGenerated[0].product_id));
  }

  render() {
    return (
      <div>
        <Card>
          <Table tablestriped="table-striped" tablebordered="table-bordered">
            <thead>
              <tr>
                <th>Produto</th>
                <th>Cobertura</th>
                <th>Valor</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {this.props.productMatch.map(product => 
                product.coverages.map(coverage => 
                  <tr key={product.id}>
                    <td>{product.name}</td>
                    <td>{coverage.display_name_ptbr}</td>
                    <td>{coverage.coverage_value}</td>
                    <td><button className="btn btn-outline-primary" onClick={() => this.handlePlan(coverage.coverage_id)} >Comprar</button></td>
                  </tr>  
                )  
              )}
            </tbody>
          </Table>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = store => ({
  products: store.base.products,
  quotationGenerated: store.quotation.quotationGenerated,
  productMatch: store.quotation.productMatch
});

const mapDispatchToProps = dispatch => ({
  setProducts: (value) => dispatch(baseActions.setProducts(value)),
  setProductMatch: (value) => dispatch(quotationActions.setProductMatch(value)),
  setPlanIdSelected: (value) => dispatch(purchaseActions.setPlanIdSelected(value))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps) (TableQuotation));