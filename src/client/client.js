import axios from 'axios';

const url = 'https://demo.assisttrip.com.br/api/';

export const client = axios.create({
	baseURL: url,
	auth: {
		username: 'demo',
		password: '3#2StZT$5ErnGYZU'
	},
	headers: {
		'Content-Type': 'application/json'
	}
});
