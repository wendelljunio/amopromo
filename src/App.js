import React from 'react';
import { Route, Switch } from "react-router-dom";
import Quotation from "./pages/Quotation";
import NavBar from "./components/NavBar";
import Purchase from './pages/Purchase';

function App() {
  return (
    <div>
      <header className="App-header">
        <NavBar title="AMO Promo"></NavBar>
      </header>
      <main>
        <div className="container">
          <Switch>
            <Route path="/cotacao" component={Quotation} />
            <Route path="/compra" component={Purchase} />
          </Switch>
        </div>
      </main>
    </div>
  );
}

export default App;
